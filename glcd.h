#ifndef _GLCD_H_
#define _GLCD_H_

#include <string>


typedef struct
{
    std::string  ConfigName;
    std::string  DisplayName;
    std::string  SkinFileName;
    bool         UpsideDown;
    bool         Invert;
    int          Brightness;
} GlcdOptions_t;


bool GlcdInit(const GlcdOptions_t * Options);
void GlcdTerm(void);
void GlcdProcess(bool Update);
void GlcdToggleInfo(void);
void GlcdSetBrightness(uint32_t Brightness);

#endif // _GLCD_H_
