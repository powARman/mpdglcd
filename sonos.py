#!/usr/bin/python3

import sys
import soco

devices = soco.discover()

for device in devices:
    favorites = device.get_sonos_favorites()['favorites']
    break

def get_device_by_name(name):
    for device in devices:
        if device.player_name == name:
            return device
    return 0

def get_fav_by_title(title):
    for fav in favorites:
        if fav['title'] == title:
            return fav
    return 0

def inc_volume(name):
    device = get_device_by_name(name)
    if device != 0:
        device.volume += 4

def dec_volume(name):
    device = get_device_by_name(name)
    if device != 0:
        device.volume -= 4

def toggle_play(name):
    device = get_device_by_name(name)
    if device != 0:
        state = device.get_current_transport_info()['current_transport_state']
        print(state)
        if state == "PLAYING":
            device.stop()
        else:
            device.play()

def play_favorite(name, title="MPD"):
    device = get_device_by_name(name)
    if device != 0:
        fav = get_fav_by_title(title)
        print(title)
        print(fav)
        if fav != 0:
            device.play_uri(fav['uri'], fav['meta'], fav['title'])
