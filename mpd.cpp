#include <stdio.h>
#include <stdint.h>

#include <mpd/client.h>

#include "mpd.h"


static MpdOptions_t MpdOptions;
static struct mpd_connection * MpdConnection;

static mpd_state    MpdState = MPD_STATE_UNKNOWN;
static std::string  MpdArtist = "";
static std::string  MpdTitle = "";
static std::string  MpdAlbum = "";
static std::string  MpdDate = "";
static uint32_t     MpdElapsedTime = 0;
static uint32_t     MpdTotalTime = 0;
static uint32_t     MpdSongPos = 0;
static bool         MpdRandom = false;
static bool         MpdRepeat = false;


static struct mpd_connection * MpdConnectionOpen(const std::string & Host, uint16_t Port)
{
    struct mpd_connection * connection;
    mpd_error error;

    connection = mpd_connection_new(Host.c_str(), Port, 1000);
    if (! connection)
    {
        fprintf(stderr, "mpd_connection_new failed\n");
        return NULL;
    }

    error = mpd_connection_get_error(connection);
    if (error != MPD_ERROR_SUCCESS)
    {
        fprintf(stderr, "mpd_connection_new error %s\n",
            mpd_connection_get_error_message(connection));
        mpd_connection_free(connection);
        return NULL;
    }
    return connection;
}


static void MpdConnectionClose(struct mpd_connection * Connection)
{
    if (Connection)
        mpd_connection_free(Connection);
}


static bool MpdConnectionCheck(struct mpd_connection * Connection)
{
    mpd_error error;

    if (! Connection)
        return false;

    error = mpd_connection_get_error(Connection);
    if (error != MPD_ERROR_SUCCESS)
    {
        fprintf(stderr, "mpd_connection error %s\n",
            mpd_connection_get_error_message(Connection));
        return false;
    }
    return true;
}


static bool MpdUpdateStatus(struct mpd_connection * Connection)
{
    if (   ! mpd_command_list_begin(Connection, true)
        || ! mpd_send_status(Connection)
        || ! mpd_send_current_song(Connection)
        || ! mpd_command_list_end(Connection))
    {
        return false;
    }

    struct mpd_status * status = mpd_recv_status(Connection);
    if (status == NULL)
    {
        return false;
    }

    mpd_state   newState = MPD_STATE_UNKNOWN;
    std::string newArtist = "";
    std::string newTitle = "";
    std::string newAlbum = "";
    std::string newDate = "";
    uint32_t    newElapsedTime = 0;
    uint32_t    newTotalTime = 0;
    uint32_t    newSongPos = 0;
    bool        newRandom = false;
    bool        newRepeat = false;

    newState = mpd_status_get_state(status);
    if (newState == MPD_STATE_PLAY || newState == MPD_STATE_PAUSE)
    {
        if (! mpd_response_next(Connection))
        {
            return false;
        }

        struct mpd_song * song = mpd_recv_song(Connection);
        if (song != NULL)
        {
            const char * tag;

            tag = mpd_song_get_tag(song, MPD_TAG_ARTIST, 0);
            if (tag)
                newArtist = tag;
            tag = mpd_song_get_tag(song, MPD_TAG_TITLE, 0);
            if (tag)
                newTitle = tag;
            tag = mpd_song_get_tag(song, MPD_TAG_ALBUM, 0);
            if (tag)
                newAlbum = tag;
            tag = mpd_song_get_tag(song, MPD_TAG_DATE, 0);
            if (tag)
                newDate = tag;
            mpd_song_free(song);
        }

        newElapsedTime = mpd_status_get_elapsed_time(status);
        newTotalTime = mpd_status_get_total_time(status);
        newSongPos = mpd_status_get_song_pos(status);
        newRandom = mpd_status_get_random(status);
        newRepeat = mpd_status_get_repeat(status);
    }

    mpd_status_free(status);

    if (! mpd_response_finish(Connection))
    {
        return false;
    }

    bool update = false;
    if (MpdState != newState)
    {
        MpdState = newState;
        update = true;
    }
    if (MpdElapsedTime != newElapsedTime)
    {
        MpdElapsedTime = newElapsedTime;
        update = true;
    }
    if (MpdTotalTime != newTotalTime)
    {
        MpdTotalTime = newTotalTime;
        update = true;
    }
    if (MpdArtist != newArtist)
    {
        MpdArtist = newArtist;
        update = true;
    }
    if (MpdTitle != newTitle)
    {
        MpdTitle = newTitle;
        update = true;
    }
    if (MpdAlbum != newAlbum)
    {
        MpdAlbum = newAlbum;
        update = true;
    }
    if (MpdDate != newDate)
    {
        MpdDate = newDate;
        update = true;
    }
    if (MpdSongPos != newSongPos)
    {
        MpdSongPos = newSongPos;
        update = true;
    }
    if (MpdRandom != newRandom)
    {
        MpdRandom = newRandom;
        update = true;
    }
    if (MpdRepeat != newRepeat)
    {
        MpdRepeat = newRepeat;
        update = true;
    }

    return update;
}


bool MpdInit(const MpdOptions_t * Options)
{
    MpdOptions = *Options;
    MpdConnection = NULL;

    return true;
}


void MpdTerm(void)
{
    MpdConnectionClose(MpdConnection);
}


bool MpdProcess(void)
{
    bool update = false;

    if (! MpdConnectionCheck(MpdConnection))
    {
        MpdConnectionClose(MpdConnection);
        MpdConnection = MpdConnectionOpen(MpdOptions.Host, MpdOptions.Port);
    }
    if (MpdConnection)
        update = MpdUpdateStatus(MpdConnection);

    return update;
}

std::string MpdGetState(void)
{
    if (MpdState == MPD_STATE_PLAY)
        return "play";
    if (MpdState == MPD_STATE_PAUSE)
        return "pause";
    return "stop";
}

std::string MpdGetArtist(void)
{
    return MpdArtist;
}

std::string MpdGetTitle(void)
{
    return MpdTitle;
}

std::string MpdGetAlbum(void)
{
    return MpdAlbum;
}

std::string MpdGetDate(void)
{
    return MpdDate;
}

uint32_t MpdGetElapsedTime(void)
{
    return MpdElapsedTime;
}

uint32_t MpdGetTotalTime(void)
{
    return MpdTotalTime;
}

bool MpdIsRandom(void)
{
    return MpdRandom;
}

bool MpdIsRepeat(void)
{
    return MpdRepeat;
}


void MpdCmdPlay(void)
{
    if (MpdConnection)
    {
        mpd_run_play(MpdConnection);
    }
}

void MpdCmdPause(void)
{
    if (MpdConnection)
    {
        mpd_run_toggle_pause(MpdConnection);
    }
}

void MpdCmdStop(void)
{
    if (MpdConnection)
    {
        mpd_run_stop(MpdConnection);
    }
}

void MpdCmdRewind(void)
{
    if (MpdConnection)
    {
        if (MpdTotalTime > 0)
        {
            if (MpdElapsedTime - 30 >= 0)
            {
                mpd_run_seek_pos(MpdConnection, MpdSongPos, MpdElapsedTime - 30);
            }
        }
    }
}

void MpdCmdFastForward(void)
{
    if (MpdConnection)
    {
        if (MpdTotalTime > 0)
        {
            if (MpdElapsedTime + 30 < MpdTotalTime)
            {
                mpd_run_seek_pos(MpdConnection, MpdSongPos, MpdElapsedTime + 30);
            }
        }
    }
}

void MpdCmdPrevious(void)
{
    if (MpdConnection)
    {
        mpd_run_previous(MpdConnection);
    }
}

void MpdCmdNext(void)
{
    if (MpdConnection)
    {
        mpd_run_next(MpdConnection);
    }
}

void MpdCmdRandom(void)
{
    if (MpdConnection)
    {
        mpd_run_random(MpdConnection, MpdRandom ? 0 : 1);
    }
}

void MpdCmdRepeat(void)
{
    if (MpdConnection)
    {
        mpd_run_repeat(MpdConnection, MpdRepeat ? 0 : 1);
    }
}
