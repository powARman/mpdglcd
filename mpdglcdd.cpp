/*
 * mpdglcdd
 *
 * mpdglcdd.c  -  mpd client with graphlcd output
 *
 * This file is released under the GNU General Public License. Refer
 * to the COPYING file distributed with this package.
 *
 * (c) 2014 Andreas Regel <andreas.regel AT powarman.de>
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <signal.h>
#include <unistd.h>

#include <string>

#include "glcd.h"
#include "lirc.h"
#include "mpd.h"
#include "sonos.h"


static const std::string kProgramName = "mpdglcdd";

static GlcdOptions_t  GlcdOptions;
static LircOptions_t  LircOptions;
static SonosOptions_t SonosOptions;
static MpdOptions_t   MpdOptions;
static uint32_t       CurrentBrightness;
static bool           InStandby = false;

static bool Quit = false;

static void HandleTermSignal(int SignalNumber)
{
    Quit = true;
}


static void PrintUsage(void)
{
    fprintf(stdout, "Usage: %s [OPTION]\n", kProgramName.c_str());
    fprintf(stdout, "\n");
    fprintf(stdout, "Options:\n");
    fprintf(stdout, "  -c,--config=CONFIG         location of the config file\n");
    fprintf(stdout, "                             (default: /etc/graphlcd.conf)\n");
    fprintf(stdout, "  -d,--display=DISPLAY       the output display (default is the first one)\n");
    fprintf(stdout, "  -s,--skin=SKIN             file name of the skin\n");
    fprintf(stdout, "  -u,--upsidedown            rotates the output by 180 degrees (default: no)\n");
    fprintf(stdout, "  -i,--invert                inverts the output (default: no)\n");
    fprintf(stdout, "  -b,--brightness=BRIGHTNESS set brightness for display if driver supports it [%%]\n");
    fprintf(stdout, "                             (default: config file value)\n");
    fprintf(stdout, "  -h,--host=HOST             host name of the mpd (default: localhost)\n");
    fprintf(stdout, "  -p,--port=PORT             port of the mpd (default: 6600)\n");
}


static bool ReadOptions(int NumArguments, char ** Arguments)
{
    const struct option longOptions[] =
    {
        {"config",     required_argument, NULL, 'c'},
        {"display",    required_argument, NULL, 'd'},
        {"skin",       required_argument, NULL, 's'},
        {"upsidedown",       no_argument, NULL, 'u'},
        {"invert",           no_argument, NULL, 'i'},
        {"brightness", required_argument, NULL, 'b'},
        {"host",       required_argument, NULL, 'h'},
        {"port",       required_argument, NULL, 'p'},
        {NULL}
    };
    int c;
    int optionIndex = 0;

    // set defaults
    GlcdOptions.ConfigName = "";
    GlcdOptions.DisplayName = "";
    GlcdOptions.SkinFileName = "";
    GlcdOptions.UpsideDown = false;
    GlcdOptions.Invert = false;
    GlcdOptions.Brightness = -1;

    LircOptions.SocketName = "/var/run/lirc/lircd";

    SonosOptions.PythonPath = "/home/pi/src/mpdglcdd/";
    SonosOptions.PythonPackage = "sonos";

    MpdOptions.Host = "localhost";
    MpdOptions.Port = 6600;

    while ((c = getopt_long(NumArguments, Arguments, "c:d:s:uib:h:p:", longOptions, &optionIndex)) != -1)
    {
        switch (c)
        {
            case 'c':
                GlcdOptions.ConfigName = optarg;
                break;

            case 'd':
                GlcdOptions.DisplayName = optarg;
                break;

            case 's':
                GlcdOptions.SkinFileName = optarg;
                break;

            case 'u':
                GlcdOptions.UpsideDown = true;
                break;

            case 'i':
                GlcdOptions.Invert = true;
                break;

            case 'b':
                GlcdOptions.Brightness = atoi(optarg);
                if (GlcdOptions.Brightness < 0)
                    GlcdOptions.Brightness = 0;
                if (GlcdOptions.Brightness > 100)
                    GlcdOptions.Brightness = 100;
                break;

            case 'h':
                MpdOptions.Host = optarg;
                break;

            case 'p':
                MpdOptions.Port = strtoul(optarg, NULL, 10);
                break;

            default:
                return false;
        }
    }

    return true;
}


int main(int argc, char ** argv)
{
    if (! ReadOptions(argc, argv))
    {
        PrintUsage();
        return EINVAL;
    }

    signal(SIGTERM, HandleTermSignal);
    signal(SIGINT, HandleTermSignal);

    CurrentBrightness = GlcdOptions.Brightness;

    if (! GlcdInit(&GlcdOptions))
    {
        return EINVAL;
    }

    if (! LircInit(&LircOptions))
    {
        return EINVAL;
    }

    if (! SonosInit(&SonosOptions))
    {
        return EINVAL;
    }

    if (! MpdInit(&MpdOptions))
    {
        return EINVAL;
    }

    while (! Quit)
    {
        LircKey_t keyCode;
        bool update;

        keyCode = LircProcess();
        if (keyCode != LIRC_KEY_INVALID)
        {
            //printf("key %d\n", keyCode);
            switch (keyCode)
            {
                case LIRC_KEY_POWER:
                    if (! InStandby)
                    {
                        GlcdSetBrightness(0);
                        InStandby = true;
                    }
                    else
                    {
                        GlcdSetBrightness(CurrentBrightness);
                        InStandby = false;
                    }
                    break;

                case LIRC_KEY_PLAY:
                    MpdCmdPlay();
                    break;

                case LIRC_KEY_PAUSE:
                    MpdCmdPause();
                    break;

                case LIRC_KEY_STOP:
                    MpdCmdStop();
                    break;

                case LIRC_KEY_REWIND:
                    MpdCmdRewind();
                    break;

                case LIRC_KEY_FASTFORWARD:
                    MpdCmdFastForward();
                    break;

                case LIRC_KEY_PREVIOUS:
                    MpdCmdPrevious();
                    break;

                case LIRC_KEY_NEXT:
                    MpdCmdNext();
                    break;

                case LIRC_KEY_RANDOM:
                    MpdCmdRandom();
                    break;

                case LIRC_KEY_REPEAT:
                    MpdCmdRepeat();
                    break;

                case LIRC_KEY_INFO:
                    GlcdToggleInfo();
                    break;

                case LIRC_KEY_RED:
                    SonosCall("play_favorite", "Küche", "MPD");
                    break;

                case LIRC_KEY_GREEN:
                    SonosCall("play_favorite", "Schlafzimmer", "MPD");
                    break;

                case LIRC_KEY_1:
                    SonosCall("toggle_play", "Küche", NULL);
                    break;
                case LIRC_KEY_2:
                    SonosCall("inc_volume", "Küche", NULL);
                    break;
                case LIRC_KEY_3:
                    SonosCall("dec_volume", "Küche", NULL);
                    break;
                case LIRC_KEY_4:
                    SonosCall("toggle_play", "Schlafzimmer", NULL);
                    break;
                case LIRC_KEY_5:
                    SonosCall("inc_volume", "Schlafzimmer", NULL);
                    break;
                case LIRC_KEY_6:
                    SonosCall("dec_volume", "Schlafzimmer", NULL);
                    break;

                case LIRC_KEY_0:
                    CurrentBrightness = 0;
                    GlcdSetBrightness(CurrentBrightness);
                    break;
                case LIRC_KEY_7:
                    CurrentBrightness = 20;
                    GlcdSetBrightness(CurrentBrightness);
                    break;
                case LIRC_KEY_8:
                    CurrentBrightness = 60;
                    GlcdSetBrightness(CurrentBrightness);
                    break;
                case LIRC_KEY_9:
                    CurrentBrightness = 100;
                    GlcdSetBrightness(CurrentBrightness);
                    break;

                case LIRC_KEY_INVALID:
                    break;
            }
        }

        update = MpdProcess();

        GlcdProcess(update);

        usleep(20000);
    }

    MpdTerm();
    SonosTerm();
    LircTerm();
    GlcdTerm();

    return 0;
}
