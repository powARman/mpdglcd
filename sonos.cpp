#include <stdio.h>
#include <stdint.h>

#include <Python.h>

#include "sonos.h"

static PyObject * pModule = NULL;

bool SonosInit(const SonosOptions_t * Options)
{
    PyObject * pName;
    std::string command;

    command = "sys.path.append(\"";
    command += Options->PythonPath;
    command += "\")";

    Py_Initialize();
    PyRun_SimpleString("import sys");
    PyRun_SimpleString(command.c_str());

    pName = PyUnicode_FromString(Options->PythonPackage.c_str());

    pModule = PyImport_Import(pName);
    Py_DECREF(pName);

    if (pModule != NULL)
    {
        return true;
    }
    PyErr_Print();
    fprintf(stderr, "Failed to load \"%s\"\n", Options->PythonPackage.c_str());
    return false;
}

void SonosTerm(void)
{
    if (pModule)
        Py_DECREF(pModule);

    Py_Finalize();
}

bool SonosCall(const char * pFunction, const char * pDevice, const char * pArgument)
{
    PyObject * pFunc;
    bool ret = false;

    if (!pModule)
        return false;

    if (!pFunction)
        return false;
    if (!pDevice)
        return false;

    pFunc = PyObject_GetAttrString(pModule, pFunction);
    /* pFunc is a new reference */

    if (pFunc && PyCallable_Check(pFunc))
    {
        PyObject * pArgs;
        int numArguments = 1;

        if (pArgument)
            numArguments++;

        pArgs = PyTuple_New(numArguments);
        if (pArgs)
        {
            PyObject * pValue;

            pValue = PyUnicode_FromString(pDevice);
            if (pValue)
            {
                /* pValue reference stolen here: */
                PyTuple_SetItem(pArgs, 0, pValue);
            }
            if (pArgument)
            {
                pValue = PyUnicode_FromString(pArgument);
                if (pValue)
                {
                    /* pValue reference stolen here: */
                    PyTuple_SetItem(pArgs, 1, pValue);
                }
            }
            pValue = PyObject_CallObject(pFunc, pArgs);
            Py_DECREF(pArgs);
            if (pValue)
            {
                Py_DECREF(pValue);
                ret = true;
            }
            else
            {
                PyErr_Print();
                fprintf(stderr,"Call failed\n");
            }
        }
    }
    else
    {
        if (PyErr_Occurred())
            PyErr_Print();
        fprintf(stderr, "Cannot find function \"%s\"\n", pFunction);
    }
    if (pFunc)
        Py_XDECREF(pFunc);

    return ret;
}
