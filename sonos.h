#ifndef _SONOS_H_
#define _SONOS_H_

#include <stdint.h>

#include <string>


typedef struct
{
    std::string  PythonPath;
    std::string  PythonPackage;
} SonosOptions_t;


bool SonosInit(const SonosOptions_t * Options);
void SonosTerm(void);
bool SonosCall(const char * pFunction, const char * pDevice, const char * pArgument);

#endif // _SONOS_H_
