#ifndef _MPD_H_
#define _MPD_H_

#include <stdint.h>

#include <string>


typedef struct
{
    std::string  Host;
    uint16_t     Port;
} MpdOptions_t;


bool MpdInit(const MpdOptions_t * Options);
void MpdTerm(void);
bool MpdProcess(void);

std::string MpdGetState(void);
std::string MpdGetArtist(void);
std::string MpdGetTitle(void);
std::string MpdGetAlbum(void);
std::string MpdGetDate(void);
uint32_t MpdGetElapsedTime(void);
uint32_t MpdGetTotalTime(void);
bool MpdIsRandom(void);
bool MpdIsRepeat(void);

void MpdCmdPlay(void);
void MpdCmdPause(void);
void MpdCmdStop(void);
void MpdCmdRewind(void);
void MpdCmdFastForward(void);
void MpdCmdPrevious(void);
void MpdCmdNext(void);
void MpdCmdRandom(void);
void MpdCmdRepeat(void);

#endif // _MPD_H_
