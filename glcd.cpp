#include <stdio.h>
#include <stdint.h>

#include <glcdgraphics/bitmap.h>
#include <glcdgraphics/font.h>
#include <glcddrivers/config.h>
#include <glcddrivers/driver.h>
#include <glcddrivers/drivers.h>
#include <glcdskin/parser.h>
#include <glcdskin/skin.h>
#include <glcdskin/config.h>
#include <glcdskin/string.h>

#include "glcd.h"
#include "mpd.h"


enum
{
    tokPlayState,
    tokIsRandom,
    tokIsRepeat,
    tokSongArtist,
    tokSongTitle,
    tokSongAlbum,
    tokSongDate,
    tokSongProgress,
    tokSongTimeElapsed,
    tokSongTimeTotal,

    tokScrollMode,
    tokScrollSpeed,
    tokScrollTime,

    tokScreenWidth,
    tokScreenHeight,

    tokCountToken
};

static const std::string kDefaultConfigFile = "/etc/graphlcd.conf";

static const std::string kSkinTokens[tokCountToken] =
{
    "PlayState",
    "IsRandom",
    "IsRepeat",
    "SongArtist",
    "SongTitle",
    "SongAlbum",
    "SongDate",
    "SongProgress",
    "SongTimeElapsed",
    "SongTimeTotal",

    "ScrollMode",
    "ScrollSpeed",
    "ScrollTime",

    "ScreenWidth",
    "ScreenHeight",
};


class cGlcdSkinConfig : public GLCD::cSkinConfig
{
private:
    GLCD::cDriver * mDriver;
public:
    cGlcdSkinConfig(GLCD::cDriver * Driver);
    virtual std::string CharSet(void);
    virtual GLCD::cType GetToken(const GLCD::tSkinToken & Token);
    virtual int GetTokenId(const std::string & Name);
    virtual GLCD::cDriver * GetDriver(void) const { return mDriver; }
};

cGlcdSkinConfig::cGlcdSkinConfig(GLCD::cDriver * Driver)
:   mDriver(Driver)
{
}

std::string cGlcdSkinConfig::CharSet(void)
{
    return "UTF-8";
}

GLCD::cType cGlcdSkinConfig::GetToken(const GLCD::tSkinToken & Token)
{
    switch (Token.Id)
    {
        case tokPlayState:
            return MpdGetState();

        case tokIsRandom:
            return MpdIsRandom();

        case tokIsRepeat:
            return MpdIsRepeat();

        case tokSongArtist:
            return MpdGetArtist();

        case tokSongTitle:
            return MpdGetTitle();

        case tokSongAlbum:
            return MpdGetAlbum();

        case tokSongDate:
            return MpdGetDate();

        case tokSongProgress:
        {
            uint32_t totalTime = MpdGetTotalTime();
            uint32_t elapsedTime = MpdGetElapsedTime();

            if (totalTime > 0)
                return (int) (elapsedTime * 100 / totalTime);
            return 0;
        }

        case tokSongTimeElapsed:
        {
            uint32_t totalTime = MpdGetTotalTime();
            uint32_t elapsedTime = MpdGetElapsedTime();

            if (totalTime > 0)
            {
                char timeString[8];

                sprintf(timeString, "%d:%02d",
                        elapsedTime / 60, elapsedTime % 60);

                return std::string(timeString);
            }
            return "";
        }

        case tokSongTimeTotal:
        {
            uint32_t totalTime = MpdGetTotalTime();

            if (totalTime > 0)
            {
                char timeString[8];

                sprintf(timeString, "%d:%02d",
                        totalTime / 60, totalTime % 60);

                return std::string(timeString);
            }
            return "";
        }

        case tokScrollMode:
            return 0;
        case tokScrollSpeed:
            return 2;
        case tokScrollTime:
            return 100;

        case tokScreenWidth:
            return mDriver->Width();
        case tokScreenHeight:
            return mDriver->Height();
    }

    return "";
}

int cGlcdSkinConfig::GetTokenId(const std::string & Name)
{
    int i;

    for (i = 0; i < tokCountToken; i++)
    {
        if (Name == kSkinTokens[i])
            return i;
    }
    fprintf(stderr, "ERROR: Unknown token %s\n", Name.c_str());
    return tokCountToken;
}


static GlcdOptions_t     GlcdOptions;
static GLCD::cDriver *   GlcdDisplay;
static GLCD::cBitmap *   GlcdScreen;
static cGlcdSkinConfig * GlcdSkinConfig;
static GLCD::cSkin *     GlcdSkin;
static std::string       GlcdDisplayId;
static uint64_t          GlcdLastUpdateTime;
static std::string       GlcdLastState;
static uint64_t          GlcdStateMessageTime;

bool GlcdInit(const GlcdOptions_t * Options)
{
    GlcdOptions = *Options;

    if (GlcdOptions.ConfigName.length() == 0)
    {
        GlcdOptions.ConfigName = kDefaultConfigFile;
        fprintf(stdout,
            "WARNING: No config file specified, using default (%s).\n",
            GlcdOptions.ConfigName.c_str());
    }

    if (GlcdOptions.SkinFileName.length() == 0)
    {
        fprintf(stderr, "ERROR: No skin file given.\n");
        return false;
    }

    uint32_t displayNumber;

    if (GLCD::Config.Load(GlcdOptions.ConfigName) == false)
    {
        fprintf(stderr, "Error loading config file!\n");
        return false;
    }

    if (GLCD::Config.driverConfigs.size() > 0)
    {
        if (GlcdOptions.DisplayName.length() > 0)
        {
            for (displayNumber = 0; displayNumber < GLCD::Config.driverConfigs.size(); displayNumber++)
            {
                if (GLCD::Config.driverConfigs[displayNumber].name == GlcdOptions.DisplayName)
                    break;
            }
            if (displayNumber == GLCD::Config.driverConfigs.size())
            {
                fprintf(stderr, "ERROR: Specified display %s not found in config file!\n", GlcdOptions.DisplayName.c_str());
                return false;
            }
        }
        else
        {
            fprintf(stdout, "WARNING: No display specified, using first one.\n");
            displayNumber = 0;
        }
    }
    else
    {
        fprintf(stderr, "ERROR: No displays specified in config file!\n");
        return false;
    }

    GLCD::Config.driverConfigs[displayNumber].upsideDown ^= GlcdOptions.UpsideDown;
    GLCD::Config.driverConfigs[displayNumber].invert ^= GlcdOptions.Invert;
    if (GlcdOptions.Brightness != -1)
        GLCD::Config.driverConfigs[displayNumber].brightness = GlcdOptions.Brightness;

    GlcdDisplay = GLCD::CreateDriver(GLCD::Config.driverConfigs[displayNumber].id,
                                 &GLCD::Config.driverConfigs[displayNumber]);
    if (!GlcdDisplay)
    {
        fprintf(stderr, "ERROR: Failed creating display object %s\n", GlcdOptions.DisplayName.c_str());
        return false;
    }
    if (GlcdDisplay->Init() != 0)
    {
        fprintf(stderr, "ERROR: Failed initializing display %s\n", GlcdOptions.DisplayName.c_str());
        delete GlcdDisplay;
        return false;
    }

    GlcdDisplay->Clear();
    GlcdDisplay->Refresh(true);

    GlcdScreen = new GLCD::cBitmap(GlcdDisplay->Width(), GlcdDisplay->Height());
    GlcdScreen->Clear();

    GlcdSkinConfig = new cGlcdSkinConfig(GlcdDisplay);
    GlcdSkin = GLCD::XmlParse(*GlcdSkinConfig, "mpdglcdd", GlcdOptions.SkinFileName);
    GlcdSkin->SetBaseSize(GlcdScreen->Width(), GlcdScreen->Height());

    GlcdDisplayId = "Normal";
    GlcdLastUpdateTime = 0;

    GlcdLastState = "";
    GlcdStateMessageTime = 0;

    return true;
}


void GlcdTerm(void)
{
    delete GlcdSkin;
    delete GlcdSkinConfig;

    delete GlcdScreen;

    GlcdDisplay->Clear();
    GlcdDisplay->Refresh(true);
    GlcdDisplay->DeInit();
    delete GlcdDisplay;
}


void GlcdProcess(bool Update)
{
    GLCD::cSkinDisplay * skinDisplay = GlcdSkin->GetDisplay(GlcdDisplayId);
    if (skinDisplay)
    {
        uint64_t currentTime = GlcdSkinConfig->Now();

        if (skinDisplay->NeedsUpdate(currentTime))
            Update = true;
        else if ((currentTime - GlcdLastUpdateTime) > 1000)
            Update = true;

        std::string mpdState = MpdGetState();
        if (GlcdLastState != mpdState)
        {
            GlcdStateMessageTime = GlcdSkinConfig->Now();
            GlcdLastState = mpdState;
        }

        if (Update)
        {
            GlcdScreen->Clear();
            skinDisplay->Render(GlcdScreen);

            if (GlcdStateMessageTime > 0)
            {
                if (GlcdSkinConfig->Now() - GlcdStateMessageTime > 2000)
                {
                    GlcdStateMessageTime = 0;
                }
                else
                {
                    GLCD::cSkinDisplay * msgDisplay = GlcdSkin->GetDisplay("StateMessage");
                    if (msgDisplay)
                    {
                        msgDisplay->Render(GlcdScreen);
                    }
                }
            }

            GlcdDisplay->SetScreen(GlcdScreen->Data(), GlcdScreen->Width(), GlcdScreen->Height());
            GlcdDisplay->Refresh(false);

            Update = false;
            GlcdLastUpdateTime = currentTime;
        }
    }
}


void GlcdToggleInfo(void)
{
    if (GlcdDisplayId == "Normal")
        GlcdDisplayId = "Info";
    else
        GlcdDisplayId = "Normal";
    GlcdLastUpdateTime = 0;
}


void GlcdSetBrightness(uint32_t Brightness)
{
    GlcdDisplay->SetBrightness(Brightness);
}
