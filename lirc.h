#ifndef _LIRC_H_
#define _LIRC_H_

#include <string>


typedef enum
{
    LIRC_KEY_POWER,
    LIRC_KEY_PLAY,
    LIRC_KEY_PAUSE,
    LIRC_KEY_STOP,
    LIRC_KEY_REWIND,
    LIRC_KEY_FASTFORWARD,
    LIRC_KEY_PREVIOUS,
    LIRC_KEY_NEXT,
    LIRC_KEY_REPEAT,
    LIRC_KEY_RANDOM,
    LIRC_KEY_INFO,
    LIRC_KEY_RED,
    LIRC_KEY_GREEN,
    LIRC_KEY_YELLOW,
    LIRC_KEY_BLUE,
    LIRC_KEY_0,
    LIRC_KEY_1,
    LIRC_KEY_2,
    LIRC_KEY_3,
    LIRC_KEY_4,
    LIRC_KEY_5,
    LIRC_KEY_6,
    LIRC_KEY_7,
    LIRC_KEY_8,
    LIRC_KEY_9,
    LIRC_KEY_INVALID
} LircKey_t;

typedef struct
{
    std::string  SocketName;
} LircOptions_t;


bool LircInit(const LircOptions_t * Options);
void LircTerm(void);
LircKey_t LircProcess(void);

#endif // _LIRC_H_
