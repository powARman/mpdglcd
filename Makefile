#
# Makefile for the mpdglcdd
#

PRGNAME = mpdglcdd

OBJS = mpdglcdd.o glcd.o lirc.o mpd.o sonos.o


### The C compiler and options:

CXX=g++

CXXFLAGS = -g -O2 -Wall -Woverloaded-virtual
#CXXFLAGS = -g -ggdb -O0 -Wall -Woverloaded-virtual
CXXFLAGS += -MMD -MP


### The directory environment:

ifndef $(DESTDIR)
    DESTDIR = /usr/local
endif

BINDIR = $(DESTDIR)/bin


### Includes and defines

INCLUDES +=

DEFINES += -D_GNU_SOURCE

CXXFLAGS += `python3.4-config --cflags`


### Libraries

LIBS += -lrt
LIBS += -lstdc++
LIBS += -lmpdclient
LIBS += -lglcddrivers -lglcdgraphics -lglcdskin

LIBDIRS +=

LDFLAGS += `python3.4-config --ldflags`


all: $(PRGNAME)
.PHONY: all

# Implicit rules:

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $(DEFINES) $(INCLUDES) $<

# Dependencies:

DEPFILE = $(OBJS:%.o=%.d)

-include $(DEPFILE)

# The main program:

$(PRGNAME): $(OBJS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(OBJS) $(LIBS) $(LIBDIRS) -o $(PRGNAME)

install: $(PRGNAME)
	install -d $(BINDIR)
	install -m 755 -o root -g root $(PRGNAME) $(BINDIR)

uninstall:
	rm -f $(BINDIR)/$(PRGNAME)

clean:
	@-rm -f $(OBJS) $(DEPFILE) $(PRGNAME) *~
