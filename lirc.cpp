#include <stdio.h>
#include <stdint.h>

#include <errno.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "lirc.h"


static LircOptions_t LircOptions;
static int           LircSocket;
static LircKey_t     LastKey;
static uint64_t      LastKeyTime;
static bool          KeyRepeat;

static struct
{
    LircKey_t KeyCode;
    const char * KeyName;
} KeyMap[] =
{
    {LIRC_KEY_POWER,        "KEY_POWER"},
    {LIRC_KEY_PLAY,         "KEY_PLAY"},
    {LIRC_KEY_PAUSE,        "KEY_PAUSE"},
    {LIRC_KEY_STOP,         "KEY_STOP"},
    {LIRC_KEY_REWIND,       "KEY_REWIND"},
    {LIRC_KEY_FASTFORWARD,  "KEY_FASTFORWARD"},
    {LIRC_KEY_PREVIOUS,     "KEY_PREVIOUS"},
    {LIRC_KEY_NEXT,         "KEY_NEXT"},
    {LIRC_KEY_REPEAT,       "KEY_RESTART"},
    {LIRC_KEY_RANDOM,       "KEY_OPTION"},
    {LIRC_KEY_INFO,         "KEY_INFO"},
    {LIRC_KEY_RED,          "KEY_RED"},
    {LIRC_KEY_GREEN,        "KEY_GREEN"},
    {LIRC_KEY_YELLOW,       "KEY_YELLOW"},
    {LIRC_KEY_BLUE,         "KEY_BLUE"},
    {LIRC_KEY_0,            "KEY_0"},
    {LIRC_KEY_1,            "KEY_1"},
    {LIRC_KEY_2,            "KEY_2"},
    {LIRC_KEY_3,            "KEY_3"},
    {LIRC_KEY_4,            "KEY_4"},
    {LIRC_KEY_5,            "KEY_5"},
    {LIRC_KEY_6,            "KEY_6"},
    {LIRC_KEY_7,            "KEY_7"},
    {LIRC_KEY_8,            "KEY_8"},
    {LIRC_KEY_9,            "KEY_9"},
    {LIRC_KEY_INVALID,      ""}
};


static uint64_t GetMs(void)
{
    struct timespec timeMs;

    clock_gettime(CLOCK_MONOTONIC, &timeMs);

    return (uint64_t) timeMs.tv_sec * 1000 + timeMs.tv_nsec / 1000000;
}


static LircKey_t GetKeyCode(const char * KeyName)
{
    LircKey_t keyCode = LIRC_KEY_INVALID;

    int i = 0;
    while (KeyMap[i].KeyCode != LIRC_KEY_INVALID)
    {
        if (strcmp(KeyMap[i].KeyName, KeyName) == 0)
        {
            keyCode = KeyMap[i].KeyCode;
            break;
        }
        i++;
    }

    return keyCode;
}


bool LircInit(const LircOptions_t * Options)
{
    struct sockaddr_un lircAddr;

    LircOptions = *Options;

    LircSocket = socket(AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0);
    if (LircSocket == -1)
    {
        fprintf(stderr, "could not create socket: %s\n", strerror(errno));
        return false;
    }

    lircAddr.sun_family = AF_UNIX;
    strcpy(lircAddr.sun_path, LircOptions.SocketName.c_str());

    if (connect(LircSocket, (struct sockaddr *)&lircAddr, sizeof(lircAddr)) == -1)
    {
        fprintf(stderr, "could not connect socket: %s\n", strerror(errno));
        return false;
    }

    LastKey = LIRC_KEY_INVALID;
    KeyRepeat = false;

    return true;
}


void LircTerm(void)
{
    close(LircSocket);
}


LircKey_t LircProcess(void)
{
    char lircBuffer[128];
    LircKey_t key = LIRC_KEY_INVALID;

    int readBytes = read(LircSocket, lircBuffer, sizeof(lircBuffer));
    if (readBytes == -1)
    {
        if (errno != EWOULDBLOCK)
        {
            fprintf(stderr, "could not read from socket: %s\n", strerror(errno));
            return LIRC_KEY_INVALID;
        }
    }
    else if (readBytes > 0)
    {
        lircBuffer[readBytes - 1] = 0;

        //printf("%s\n", lircBuffer);

        int count;
        char keyName[32];
        if (sscanf(lircBuffer, "%*x %x %31s", &count, keyName) != 2)
        {
            return LIRC_KEY_INVALID;
        }
        //printf("key %s, count %d\n", keyName, count);

        key = GetKeyCode(keyName);
        if (key != LIRC_KEY_INVALID)
        {
            if (key != LastKey)
            {
                KeyRepeat = false;
                LastKey = key;
                LastKeyTime = GetMs();
            }
            else
            {
                if (count == 0)
                {
                    KeyRepeat = false;
                    LastKey = key;
                    LastKeyTime = GetMs();
                }
                else
                {
                    if (! KeyRepeat)
                    {
                        if (GetMs() - LastKeyTime < 1500)
                        {
                            key = LIRC_KEY_INVALID;
                        }
                        else
                        {
                            KeyRepeat = true;
                            LastKeyTime = GetMs();
                        }
                    }
                    else
                    {
                        if (GetMs() - LastKeyTime < 200)
                        {
                            key = LIRC_KEY_INVALID;
                        }
                        else
                        {
                            LastKeyTime = GetMs();
                        }
                    }
                }
            }
        }
    }

    return key;
}
